import React from 'react';
import { useState, useEffect } from 'react'
import { supabase } from '../supabaseClient'
import Avatar from '../avatar'
import { Formik, Form, Field, FieldArray, TextField } from 'formik';

export default function Account({ }) {
  const [loading, setLoading] = useState(true)
  const [username, setUsername] = useState(null)
  const [birthdate, setBirthdate] = useState(null)
  const [avatar_url, setAvatarUrl] = useState(null)

  const [retire_at, setRetireat] = useState(null)
  const [workdays, setWorkdays] = useState(null)
  const [workhours, setWorkhours] = useState(null)
  const [vacationdays, setVacationdays] = useState(null)

  const session = supabase.auth.session();

  useEffect(() => {
    getProfile()
  }, [session])

  async function getProfile() {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      let { data, error, status } = await supabase
        .from('profiles')
        .select(`username, birthdate, avatar_url`)
        .eq('id', user.id)
        .single()

      if (error && status !== 406) {
        throw error
      }

      if (data) {
        setUsername(data.username)
        setBirthdate(data.birthdate)
        setAvatarUrl(data.avatar_url)
      }
    } catch (error) {
      alert('error.message')
    } finally {
      setLoading(false)
    }
  }

  async function updateProfile({ username, birthdate, avatar_url }) {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      const updates = {
        id: user.id,
        username,
        birthdate,
        avatar_url,
        updated_at: new Date(),
      }

      let { error } = await supabase.from('profiles').upsert(updates, {
        returning: 'minimal', // Don't return the value after inserting
      })

      if (error) {
        throw error
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }


 async function updateGoals({ retire_at, workdays, workhours, vacationdays }) {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      const updates = {
        user_id: user.id,
        retire_at,
        workdays,
        workhours,
        vacationdays,
        updated_at: new Date(),
      }

      let { error } = await supabase.from('goals').upsert(updates, {
        returning: 'minimal', // Don't return the value after inserting
      })

      if (error) {
        throw error
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
      const session = supabase.auth.session()
    }
  }

  return (
    <div className="container mx-auto px-4 py-8">
  		
 <div className="form-widget">
        <div className="flex flex-row">
         <Avatar
          url={avatar_url}
          size={150}
          onUpload={(url) => {
            setAvatarUrl(url)
            updateProfile({ username, birthdate, avatar_url: url })
          }}
          className="basis-1/2 px-4"
        />
        <div className="basis-1/2 px-4">
          <div>
            <label htmlFor="email">Email</label>
            <input id="email" type="text" value={session.user.email} disabled />
          </div>
          <div>
            <label htmlFor="username">Name</label>
            <input
              id="username"
              type="text"
              value={username || ''}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="birthdate">Birth date</label>
            <input
              id="birthdate"
              type="date"
              value={birthdate || ''}
              onChange={(e) => setBirthdate(e.target.value)}
            />
          </div>

          <div>
            <button
              className="button block primary mt-5"
              onClick={() => updateProfile({ username, birthdate, avatar_url })}
              disabled={loading}
            >
              {loading ? 'Loading ...' : 'Update'}
            </button>
          </div>
          </div>

          <div>
            <button className="button block" onClick={() => supabase.auth.signOut()}>
              Sign Out
            </button>
          </div>
        </div>
      </div>



    </div>
  )
}