import React from 'react';
import { useState, useEffect } from 'react'
import { supabase } from '../supabaseClient'
import Avatar from '../avatar'
import { Formik, Form, Field, FieldArray, TextField } from 'formik';



export default function Account({ session }) {
  const [loading, setLoading] = useState(true)

  const [retire_at, setRetireat] = useState(null)
  const [workdays, setWorkdays] = useState(null)
  const [workhours, setWorkhours] = useState(null)
  const [vacationdays, setVacationdays] = useState(null)

	

  useEffect(() => {
    getGoals()
  }, [session])

  async function getGoals() {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      let { data, error, status } = await supabase
        .from('goals')
        .select(`retire_at, workdays, workhours, vacationdays`)
        .eq('user_id', user.id)
        .single()

      if (error && status !== 406) {
        throw error
      }

      if (data) {
        setRetireat(data.retire_at)
        setWorkdays(data.workdays)
        setWorkhours(data.workhours)
        setVacationdays(data.vacationdays)
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

 async function updateGoals({ retire_at, workdays, workhours, vacationdays }) {
    try {
      setLoading(true)
      const user = supabase.auth.user()

      const updates = {
        user_id: user.id,
        retire_at,
        workdays,
        workhours,
        vacationdays,
        updated_at: new Date(),
      }

      let { error } = await supabase.from('goals').upsert(updates, {
        returning: 'minimal', // Don't return the value after inserting
      })

      if (error) {
        throw error
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
      const session = supabase.auth.session()
    }
  }

  return (
    <div className="container mx-auto px-4">
  		

  <div className="form-widget mt-8">
      <div className="flex flex-row">
         
        <div className="px-4">
          <div>
            <label htmlFor="retire_at">Retire in (years)</label>
            <input 
            id="retire_at" 
            type="text" 
            value={retire_at || ''}
            onChange={(e) => setRetireat(e.target.value)}
             />
          </div>
          <div>
            <label htmlFor="workdays">Work days in a month</label>
            <input
              id="workdays"
              type="text"
              value={workdays || ''}
              onChange={(e) => setWorkdays(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="workhours">Work hours in a day</label>
            <input
              id="workhours"
              type="text"
              value={workhours || ''}
              onChange={(e) => setWorkhours(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="vacationdays">No of vacation days</label>
            <input
              id="vacationdays"
              type="text"
              value={vacationdays || ''}
              onChange={(e) => setVacationdays(e.target.value)}
            />
          </div>

          <div>
            <button
              className="button block primary"
              onClick={() => updateGoals({ retire_at, workdays, workhours, vacationdays })}
              disabled={loading}
            >
              {loading ? 'Loading ...' : 'Update Goals'}
            </button>
          </div>
          </div>

        </div>
      </div>


<div className="mt-5">
  <div className="flex flex-row">
    <div className="basis-1/4 border border-gray-400 p-5 mr-2 text-center bg-gray-100 text-black">Total working hours: <br></br><span className="text-black text-xl">{ retire_at * 12 * workdays * workhours }</span></div>
    <div className="basis-1/4 border border-gray-400 p-5 mr-2 text-center bg-gray-100 text-black">Total working days:     <br></br><span className="text-black text-xl">{ retire_at * workdays }</span></div>
    <div className="basis-1/4 border border-gray-400 p-5 mr-2 text-center bg-gray-100 text-black">Total vacation days:   <br></br> <span className="text-black text-xl">{ vacationdays }</span></div>
    <div className="basis-1/4 border border-gray-400 p-5 mr-2 text-center bg-gray-100 text-black">Total nothing:    <br></br> <span className="text-black text-xl">0</span></div>
  </div>
</div>



    </div>
  )
}