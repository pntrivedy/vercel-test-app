import React from 'react';
import { useState, useEffect } from 'react'
import { supabase } from '../supabaseClient'
import Avatar from '../avatar'
import { Formik, Form, Field, FieldArray, TextField } from 'formik';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Dreams from './Dreams.js';
import Goals from './Goals.js';
import Profile from './Profile.js';

export default function Account({ session }) {
  return (
    <div className="">
    <Router>
        <nav>
          <ul className="flex">
            <li className="mr-6">
              <Link className="text-blue-500 hover:text-blue-800" to="/profile">Profile</Link>
            </li>
            <li className="mr-6">
              <Link className="text-blue-500 hover:text-blue-800" to="/goals">Goals</Link>
            </li>
            <li className="mr-6">
              <Link className="text-blue-500 hover:text-blue-800" to="/dreams">Dreams</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/goals">
            <Goals/>
          </Route>
          <Route path="/dreams">
            <Dreams/>
          </Route>
          <Route path="/profile">
            <Profile/>
          </Route>
        </Switch>
      
    </Router>
    </div>
  )
}