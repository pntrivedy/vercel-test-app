import React from 'react';
import { useState, useEffect } from 'react'
import { supabase } from '../supabaseClient'
import Avatar from '../avatar'
import { Formik, Form, Field, FieldArray, TextField } from 'formik';



export default function Account({ session }) {
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    //getDreams()
  }, [session])

  // async function getDreams() {
  //   try {
  //     setLoading(true)
  //     const user = supabase.auth.user()

  //     let { data, error, status } = await supabase
  //       .from('goals')
  //       .select(`retire_at, workdays, workhours, vacationdays`)
  //       .eq('user_id', user.id)
  //       .single()

  //     if (error && status !== 406) {
  //       throw error
  //     }

  //     if (data) {
  //       setRetireat(data.retire_at)
  //       setWorkdays(data.workdays)
  //       setWorkhours(data.workhours)
  //       setVacationdays(data.vacationdays)
  //     }
  //   } catch (error) {
  //     alert(error.message)
  //   } finally {
  //     setLoading(false)
  //   }
  // }

 // async function updateDreams({ retire_at, workdays, workhours, vacationdays }) {
 //    try {
 //      setLoading(true)
 //      const user = supabase.auth.user()

 //      const updates = {
 //        user_id: user.id,
 //        retire_at,
 //        workdays,
 //        workhours,
 //        vacationdays,
 //        updated_at: new Date(),
 //      }

 //      let { error } = await supabase.from('goals').upsert(updates, {
 //        returning: 'minimal', // Don't return the value after inserting
 //      })

 //      if (error) {
 //        throw error
 //      }
 //    } catch (error) {
 //      alert(error.message)
 //    } finally {
 //      setLoading(false)
 //      const session = supabase.auth.session()
 //    }
 //  }

  return (
    <div className="container mx-auto px-4">
  		<div className="form-widget mt-8">
    		<Formik
       			initialValues={{ dreams: [{ id: 1, title:"house", amount:1100000 }] }}
       			onSubmit={(values, { setSubmitting }) => {
         		setTimeout(() => {
           			alert(JSON.stringify(values, null, 2));
           			setSubmitting(false);
         		}, 400);
       			}}
    		>
       		{({
         		values,
         		errors,
         		handleChange,
         		handleSubmit,
         		isSubmitting,
       		}) => (
         <Form>
          
         {/* <FieldArray name="dreams" >
          {
            ({push}) => 

            (<div>
            
              <

              <button type="button" onClick={() => push({ id: 2, title:"", amount:0 })}>Add to list</button>  

            </div>)
          }
          </FieldArray>*/}

          <FieldArray
             name="dreams"
             className="flex flex-row"
             render={arrayHelpers => (
               <div>
                 {values.dreams && values.dreams.length > 0 ? (
                   values.dreams.map((friend, index) => (
                     <div key={index}>
                        <Field className="basis-1/3" type="text" name={`dreams.${index}.title`} placeholder="Name your dream" />
                        <Field className="basis-1/3" type="number" name={`dreams.${index}.amount`} />
                        <Field className="basis-1/3" type="date" name={`dreams.${index}.deadline`} />
                        

                       <button
                         type="button"
                         onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                       >
                         -
                       </button>
                       <button
                         type="button"
                         onClick={() => arrayHelpers.insert(index, '')} // insert an empty string at a position
                       >
                         +
                       </button>
                     </div>
                   ))
                 ) : (
                   <button type="button" onClick={() => arrayHelpers.push('')}>
                     {/* show this when user has removed all dreams from the list */}
                     Add a friend
                   </button>
                 )}
                 <div>
                   <button type="submit">Submit</button>
                 </div>
               </div>
             )}
           />


           <pre>
           {JSON.stringify(values, null, 2)}
           </pre>
         </Form>
       )}
     </Formik>
      </div>



    </div>
  )
}